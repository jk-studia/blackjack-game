package pl.jarekkozmic.blackjack.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import pl.jarekkozmic.blackjack.models.Player;

import java.io.IOException;

public class NewGameWindowController {

    @FXML
    private TextField nameField;
    @FXML
    private Button startButton;


    public void startButtonOnAction(ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        Player player = new Player(nameField.getText());
        FXMLLoader loader;
        if(player.getName().equals("")){
            ErrorWindowController errorWindowController = new ErrorWindowController("Name must be specified");
            loader = new FXMLLoader(getClass().getResource("/views/errorWindow.fxml"));
            loader.setController(errorWindowController);
            stage.setTitle("Error");
        }else{
            loader = new FXMLLoader(getClass().getResource("/views/blackjack.fxml"));
            BlackjackController blackjackController = new BlackjackController(player);
            loader.setController(blackjackController);
            Stage primaryStage = (Stage) startButton.getScene().getWindow();
            primaryStage.close();
            stage.setTitle("Blackjack");
        }
        stage.setResizable(false);
        stage.setScene(new Scene(loader.load()));
        stage.centerOnScreen();
        stage.show();
    }

    public void setStartButton(Button starButton) {
        this.startButton = starButton;
    }

    public void setNameField(TextField nameField) {
        this.nameField = nameField;
    }

    public TextField getNameField() {
        return nameField;
    }

    public Button getStartButton() {
        return startButton;
    }
}
