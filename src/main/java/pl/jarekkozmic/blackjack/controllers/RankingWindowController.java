package pl.jarekkozmic.blackjack.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

@SuppressWarnings("unchecked")
public class RankingWindowController implements Initializable {

    public ListView listView;

    private ObservableList observableList;

    public RankingWindowController(List playerList) {
        List<String> placeList = new ArrayList();
        Iterator iterator = playerList.iterator();
        int place = 1;
        while(iterator.hasNext()){
            placeList.add(place+". "+iterator.next());
            place++;
        }
        observableList = FXCollections.observableList(placeList);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        listView.setItems(observableList);
    }

    public void closeButtonOnAction(){
        System.exit(0);
    }
}
