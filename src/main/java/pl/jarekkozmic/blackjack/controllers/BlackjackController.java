package pl.jarekkozmic.blackjack.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import pl.jarekkozmic.blackjack.models.Game;
import pl.jarekkozmic.blackjack.models.Player;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class BlackjackController implements Initializable {

    public Label playerNameLabel;
    public Label scoreNumberLabel;
    public Label croupierScoreNumberLabel;
    public Label amountOfCreditLabel;
    public TextField betTextField;
    public Canvas canvas;
    public Label informationLabel;
    public Button betButton;
    public Button standButton;
    public Button hitButton;
    public Button doubleDownButton;

    private Timeline timeline;
    private Game game;

    public BlackjackController(Player player){
        game = new Game(player);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        playerNameLabel.setText(game.getPlayer().getName());
        amountOfCreditLabel.setText(Double.toString(game.getPlayer().getCredit()));
        scoreNumberLabel.setText(Integer.toString(game.getPlayer().getScore()));
        croupierScoreNumberLabel.setText(Integer.toString(game.getCroupier().getScore()));
        betTextField.setText("0");

        game.setGc(canvas.getGraphicsContext2D());
        game.clearBoard();

        hitButton.setDisable(true);
        standButton.setDisable(true);
        doubleDownButton.setDisable(true);
    }

    public void betButtonOnAction(ActionEvent actionEvent) {
        if(game.getPlayer().getCredit() == 0){
            betButton.setDisable(true);
            informationLabel.setText("GAME OVER");
        }else {
            game.setPlayersBet(Double.parseDouble(betTextField.getText()));
            if (game.getPlayersBet() > 0 && game.getPlayersBet() <= game.getPlayer().getCredit()) {
                game.getPlayer().setCredit(game.getPlayer().getCredit() - game.getPlayersBet());
                amountOfCreditLabel.setText(Double.toString(game.getPlayer().getCredit()));

                KeyFrame simulation = generateSimulation();
                timeline = new Timeline(simulation);
                timeline.setCycleCount(Timeline.INDEFINITE);
                timeline.play();

                betButton.setDisable(true);
            } else {
                try {
                    Stage stage = new Stage();
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/errorWindow.fxml"));
                    ErrorWindowController errorWindowController = new ErrorWindowController("Incorrect bet");
                    loader.setController(errorWindowController);
                    stage.setScene(new Scene(loader.load()));
                    stage.setTitle("Error");
                    stage.setResizable(false);
                    stage.show();
                } catch (IOException e) {
                    System.exit(1);
                }
            }
        }
    }

    private KeyFrame generateSimulation(){
        KeyFrame simulation = new KeyFrame(Duration.seconds(1.0),event -> {
            if(game.getCardsImages().size() == 0) {
                game.clearBoard();
                game.handOutNewRoundCards();
                scoreNumberLabel.setText(Integer.toString(game.getPlayer().getScore()));
                croupierScoreNumberLabel.setText(Integer.toString(game.getCroupier().getScore()));
                informationLabel.setText("");
                hitButton.setDisable(false);
                standButton.setDisable(false);
                if(game.getPlayer().getCredit()>game.getPlayersBet())
                    doubleDownButton.setDisable(false);
            }
            if(game.getPlayer().getScore() == 21 && game.getPlayer().getHand().size() == 2){
                informationLabel.setText("BLACKJACK");
                game.getPlayer().setCredit(game.getPlayer().getCredit()+2.5*game.getPlayersBet());
                amountOfCreditLabel.setText(Double.toString(game.getPlayer().getCredit()));
                timeline.stop();
                reset();
            }
            if(game.getPlayer().getScore() > 21) {
                informationLabel.setText("YOU LOSE");
                timeline.stop();
                reset();
            }
        });
        return simulation;
    }

    public void hitButtonOnAction(ActionEvent actionEvent) {
        game.hit();
        scoreNumberLabel.setText(Integer.toString(game.getPlayer().getScore()));
        doubleDownButton.setDisable(true);
    }

    public void standButtonOnAction(ActionEvent actionEvent) {
        timeline.stop();
        game.showHiddenCard();
        croupierScoreNumberLabel.setText(Integer.toString(game.getCroupier().getFullScore()));
        if(game.getPlayer().getScore() > game.getCroupier().getFullScore()){
            informationLabel.setText("YOU WIN");
            game.getPlayer().setCredit(game.getPlayer().getCredit()+2*game.getPlayersBet());
            amountOfCreditLabel.setText(Double.toString(game.getPlayer().getCredit()));
        } else if(game.getPlayer().getScore() < game.getCroupier().getFullScore()){
            informationLabel.setText("YOU LOSE");
        } else {
            informationLabel.setText("DRAW");
            game.getPlayer().setCredit(game.getPlayer().getCredit()+game.getPlayersBet());
            amountOfCreditLabel.setText(Double.toString(game.getPlayer().getCredit()));
        }
        reset();
    }

    private void reset(){
        game.resetGame();
        hitButton.setDisable(true);
        standButton.setDisable(true);
        doubleDownButton.setDisable(true);
        betButton.setDisable(false);
    }

    public void doubleDownButtonOnAction(ActionEvent actionEvent){
        game.doubleDown();
        amountOfCreditLabel.setText(Double.toString(game.getPlayer().getCredit()));
        scoreNumberLabel.setText(Integer.toString(game.getPlayer().getScore()));
        hitButton.setDisable(true);
        standButton.setDisable(true);
        doubleDownButton.setDisable(true);
        if(game.getPlayer().getScore() <= 21)
            standButtonOnAction(actionEvent);
    }

    public void newGameMenuItemOnAction(ActionEvent actionEvent){
        Stage stage = (Stage) betButton.getScene().getWindow();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/newGameWindow.fxml"));
        Stage newStage = new Stage();
        try {
            newStage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        newStage.setResizable(false);
        newStage.setTitle("Blackjack");
        newStage.show();
        stage.close();
    }

    public void closeMenuItemOnAction(ActionEvent actionEvent) {
        if(timeline != null)
             timeline.stop();
        betButton.setDisable(true);
        Stage stage = new Stage();
        SaveWindowController saveWindowController = new SaveWindowController(game.getPlayer());
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/saveWindow.fxml"));
            loader.setController(saveWindowController);
            stage.setScene(new Scene(loader.load()));
            stage.setResizable(false);
            stage.setTitle("Save");
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
