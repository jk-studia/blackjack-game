package pl.jarekkozmic.blackjack.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import pl.jarekkozmic.blackjack.models.DatabaseConnection;
import pl.jarekkozmic.blackjack.models.Player;

import java.io.IOException;

public class SaveWindowController {

    private Player player;

    public Button yesButton;

    public SaveWindowController(Player player){
        this.player = player;
    }

    public void yesButtonOnAction(ActionEvent actionEvent) {
        DatabaseConnection databaseConnection = new DatabaseConnection("/META-INF/hibernate.cfg.xml");
        databaseConnection.save(player);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/rankingWindow.fxml"));
        RankingWindowController rankingWindowController = new RankingWindowController(databaseConnection.readPlayers());
        databaseConnection.getSessionFactory().close();
        loader.setController(rankingWindowController);
        Stage stage = (Stage) yesButton.getScene().getWindow();
        try {
            stage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        stage.centerOnScreen();
        stage.setWidth(500);
        stage.setHeight(400);
        stage.setTitle("Ranking");
        stage.setOnCloseRequest(windowEvent -> System.exit(0));
        stage.show();
    }

    public void noButtonOnAction(ActionEvent actionEvent) {
        System.exit(0);
    }
}
