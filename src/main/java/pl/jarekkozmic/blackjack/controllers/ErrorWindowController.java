package pl.jarekkozmic.blackjack.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class ErrorWindowController implements Initializable {

    public Label messageLabel;
    public Button okButton;

    private String message;
    
    public ErrorWindowController( String message){
        this.message = message;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        messageLabel.setText(message);
    }

    public void okButtonOnAction(ActionEvent event){
        Stage stage = (Stage) okButton.getScene().getWindow();
        stage.close();
    }
}
