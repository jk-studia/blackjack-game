package pl.jarekkozmic.blackjack.models;

import java.util.*;

public class Deck implements Shuffleable {

    private List<Card> cards;

    public Deck(){
        cards = new LinkedList<>();

        for(int i = 1, j = 11; i <= 52; i+=4, j--){
            if(i > 4 && i <= 20)
                j = 10;
            cards.add(new Card(Suit.CLOVER, j, i+".png"));
        }
        for(int i = 2, j = 11; i <= 52; i+=4, j--) {
            if (i > 4 && i <= 20)
                j = 10;
            cards.add(new Card(Suit.PIKE, j, i + ".png"));
        }
        for(int i = 3, j = 11; i <= 52; i+=4, j--){
            if (i > 4 && i <= 20)
                j = 10;
            cards.add(new Card(Suit.HEART, j, i+".png"));
        }
        for(int i = 4, j = 11; i <= 52; i+=4, j--){
            if (i > 4 && i <= 20)
                j = 10;
            cards.add(new Card(Suit.TILE, j, i+".png"));
        }

        cards.sort(Comparator.comparing(Card::getSuit));
    }

    public void shuffle() {
        Set<Card> cardSet = new HashSet<>();
        Random random = new Random();
        do {
            for (int i = 0; i < 52; i++) {
                cardSet.add(cards.get(random.nextInt(52)));
            }
        }while(cardSet.size()<52);
        cards.clear();
        cards.addAll(cardSet);
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

}
