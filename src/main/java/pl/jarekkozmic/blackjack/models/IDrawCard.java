package pl.jarekkozmic.blackjack.models;

public interface IDrawCard {
    Deck drawCards(Deck deck);
}
