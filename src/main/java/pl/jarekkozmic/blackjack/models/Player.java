package pl.jarekkozmic.blackjack.models;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
@Table
public class Player implements IDrawCard {

    private int Id;
    private String name;
    private double credit;
    private int score;
    private List<Card> hand;

    public Player(){ }

    public Player(String name){
        this.name = name;
        this.credit = 5000;
        hand = new LinkedList<>();
        score = 0;
    }

    public Deck drawCards(Deck deck){
        Card card = ((LinkedList<Card>) deck.getCards()).poll();
        hand.add(card);
        if(score+card.getValue()>21) {
            score = 0;
            for (int i = 0; i < hand.size(); i++)
                if (hand.get(i).getValue() == 11)
                    score+=1;
                else
                    score+=hand.get(i).getValue();
        }
        else
            score+=card.getValue();
        return deck;
    }

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "credit")
    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    @Transient
    public List<Card> getHand() {
        return hand;
    }

    public void setHand(List<Card> hand) {
        this.hand = hand;
    }

    @Transient
    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return name+", credit: "+credit;
    }
}
