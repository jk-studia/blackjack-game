package pl.jarekkozmic.blackjack.models;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Game {

    private Player player;
    private Croupier croupier;
    private Deck deck;
    private Double playersBet;
    private GraphicsContext gc;
    private List<Image> cardsImages;
    private Image backImage;

    private final int CARD_WIDTH = 100;
    private final int CARD_HEIGHT = 150;
    private final int OX_OF_CARDS = 270;
    private final int DISTANCE_BETWEEN_CARDS_OX = 110;
    private final int OY_OF_CROUPIER_CARDS = 50;
    private final int OY_OF_PLAYER_CARDS = 250;

    public Game(Player player){
        this.player = player;
        croupier = new Croupier();
        playersBet = 0.0;
        deck = new Deck();
        deck.shuffle();
        cardsImages = new ArrayList<>();
        backImage = new Image("/cards/back.png");
    }

    public void handOutNewRoundCards(){
        player.drawCards(deck);
        player.drawCards(deck);
        croupier.drawCards(deck);

        cardsImages.add(new Image(croupier.getHand().get(0).getImagePath()));
        cardsImages.add(new Image(croupier.getHand().get(1).getImagePath()));
        cardsImages.add(new Image(player.getHand().get(0).getImagePath()));
        cardsImages.add(new Image(player.getHand().get(1).getImagePath()));

        // Croupier Cards
        gc.drawImage(cardsImages.get(0), OX_OF_CARDS, OY_OF_CROUPIER_CARDS, CARD_WIDTH, CARD_HEIGHT);
        gc.drawImage(backImage, OX_OF_CARDS+DISTANCE_BETWEEN_CARDS_OX, OY_OF_CROUPIER_CARDS, CARD_WIDTH,CARD_HEIGHT);

        // Player Cards
        gc.drawImage(cardsImages.get(3), OX_OF_CARDS, OY_OF_PLAYER_CARDS, CARD_WIDTH, CARD_HEIGHT);
        gc.drawImage(cardsImages.get(2), OX_OF_CARDS+DISTANCE_BETWEEN_CARDS_OX, OY_OF_PLAYER_CARDS, CARD_WIDTH, CARD_HEIGHT);

    }

    public void hit(){
        player.drawCards(deck);
        cardsImages.add(new Image(((LinkedList<Card>) player.getHand()).getLast().getImagePath()));
        for(int i = 2, j = 1; i < 2+player.getHand().size(); i++, j--){
            gc.drawImage(cardsImages.get(i), OX_OF_CARDS+j*DISTANCE_BETWEEN_CARDS_OX, OY_OF_PLAYER_CARDS, CARD_WIDTH, CARD_HEIGHT);
            if(j==-2) j = 3;
        }
    }

    public void doubleDown(){
        player.setCredit(player.getCredit()-playersBet);
        playersBet+=playersBet;
        hit();
    }

    public void showHiddenCard(){
        gc.drawImage(cardsImages.get(1),OX_OF_CARDS+DISTANCE_BETWEEN_CARDS_OX, OY_OF_CROUPIER_CARDS, CARD_WIDTH,CARD_HEIGHT);
    }

    public void clearBoard(){
        gc.setFill(Color.GREEN);
        gc.fillRect(0,0,700,500);
        gc.drawImage(backImage,50,50, CARD_WIDTH, CARD_HEIGHT);
        gc.drawImage(backImage,55,50, CARD_WIDTH, CARD_HEIGHT);
        gc.drawImage(backImage,60,50, CARD_WIDTH, CARD_HEIGHT);
    }

    public void resetGame(){
        playersBet = 0.0;
        croupier.setFullScore(0);
        croupier.setScore(0);
        player.setScore(0);
        deck.getCards().addAll(croupier.getHand());
        deck.getCards().addAll(player.getHand());
        player.getHand().clear();
        cardsImages.clear();
    }

    public Player getPlayer() {
        return player;
    }

    public Croupier getCroupier() {
        return croupier;
    }

    public Deck getDeck() {
        return deck;
    }

    public GraphicsContext getGc() {
        return gc;
    }

    public void setGc(GraphicsContext gc) {
        this.gc = gc;
    }

    public List<Image> getCardsImages() {
        return cardsImages;
    }

    public Double getPlayersBet() {
        return playersBet;
    }

    public void setPlayersBet(Double playersBet) {
        this.playersBet = playersBet;
    }
}
