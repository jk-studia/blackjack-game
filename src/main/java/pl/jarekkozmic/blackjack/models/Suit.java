package pl.jarekkozmic.blackjack.models;

public enum Suit {
    HEART,
    TILE,
    CLOVER,
    PIKE
}
