package pl.jarekkozmic.blackjack.models;


public class Card {

    private Suit suit;
    private int value;
    private String imagePath;

    public Card(Suit suit, int value, String imageName){
        this.suit = suit;
        this.value = value;
        imagePath = "/cards/" + imageName;
    }

    public Suit getSuit() {
        return suit;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public String toString() {
        return suit+" "+value+" "+imagePath;
    }
}
