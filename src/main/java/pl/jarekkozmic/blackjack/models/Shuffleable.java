package pl.jarekkozmic.blackjack.models;

public interface Shuffleable {
    void shuffle();
}
