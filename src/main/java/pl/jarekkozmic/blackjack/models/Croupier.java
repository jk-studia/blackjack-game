package pl.jarekkozmic.blackjack.models;

import java.util.LinkedList;
import java.util.List;

public class Croupier implements IDrawCard {

    private int score;
    private int fullScore;
    private List<Card> hand;

    public Croupier(){
        hand = new LinkedList<>();
        score = 0;
        fullScore = 0;
    }

    @Override
    public Deck drawCards(Deck deck) {
        Card card = ((LinkedList<Card>) deck.getCards()).poll();
        hand.add(card);
        score+=card.getValue();
        fullScore+=card.getValue();

        card = ((LinkedList<Card>) deck.getCards()).poll();
        hand.add(card);
        fullScore+=card.getValue();

        return deck;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public List<Card> getHand() {
        return hand;
    }

    public void setHand(List<Card> hand) {
        this.hand = hand;
    }

    public int getFullScore() {
        return fullScore;
    }

    public void setFullScore(int fullScore) {
        this.fullScore = fullScore;
    }
}
