package pl.jarekkozmic.blackjack.models;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.List;

public class DatabaseConnection {

    private SessionFactory sessionFactory;

    public DatabaseConnection(String configFilePath){
        Configuration configuration = new Configuration();
        configuration.configure(configFilePath);
        sessionFactory = configuration.buildSessionFactory();
    }

    public void save(Player player){
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            session.persist(player);
            transaction.commit();
        } catch(HibernateException he) {
            if(transaction!=null) transaction.rollback();
            he.printStackTrace();
        } finally {
            session.close();
        }
    }

    public List readPlayers(){
        List playerList;
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Player order by credit desc");
            playerList =  query.getResultList();
            transaction.commit();
            return playerList;
        } catch(HibernateException he){
            if(transaction!=null) transaction.rollback();
            he.printStackTrace();
            return null;
        } finally{
            session.close();
        }
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
