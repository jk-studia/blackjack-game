package pl.jarekkozmic.blackjacktest;

import org.junit.jupiter.api.Test;
import pl.jarekkozmic.blackjack.models.Card;
import pl.jarekkozmic.blackjack.models.Suit;

import static org.junit.jupiter.api.Assertions.*;

public class CardTests {

    @Test
    public void testIfCardIsCreated(){
        Card card = new Card(Suit.HEART, 11, "1.png");
        assertNotNull(card);
    }

}
