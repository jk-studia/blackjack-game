package pl.jarekkozmic.blackjacktest;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.jarekkozmic.blackjack.models.Card;
import pl.jarekkozmic.blackjack.models.Deck;
import pl.jarekkozmic.blackjack.models.Suit;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class DeckTests {

    private static Deck firstDeck;
    private static Deck secondDeck;

    @BeforeAll
    public static void setUp(){
        firstDeck = new Deck();
        secondDeck = new Deck();
    }

    @Test
    public void testIfDeckWillBeCreated(){
        assertNotNull(firstDeck);
        assertNotNull(secondDeck);
    }

    @Test
    public void testIfCardsInDeckAreCorrect(){
        List<Card> exampleDeck = staticDeck();
        for(int i = 0; i < 52; i++){
            assertEquals(exampleDeck.get(i).toString(), firstDeck.getCards().get(i).toString());
        }
    }

    @Test
    public void testIfDeckWillBeShuffled(){
        List<Card> exampleDeck = staticDeck();
        secondDeck.shuffle();
        boolean shuffled = false;
        for(int i = 0; i < 52; i++){
            if(!exampleDeck.get(i).toString().equals(secondDeck.getCards().get(i).toString())){
                shuffled = true;
                break;
            }
        }
        assertTrue(shuffled);
    }

    public  List<Card> staticDeck(){
        List<Card> exampleDeck = new LinkedList<Card>();
        exampleDeck.add(new Card(Suit.CLOVER, 11, "1.png"));
        exampleDeck.add(new Card(Suit.PIKE, 11, "2.png"));
        exampleDeck.add(new Card(Suit.HEART, 11, "3.png"));
        exampleDeck.add(new Card(Suit.TILE, 11, "4.png"));

        exampleDeck.add(new Card(Suit.CLOVER, 10, "5.png"));
        exampleDeck.add(new Card(Suit.PIKE, 10, "6.png"));
        exampleDeck.add(new Card(Suit.HEART, 10, "7.png"));
        exampleDeck.add(new Card(Suit.TILE, 10, "8.png"));

        exampleDeck.add(new Card(Suit.CLOVER, 10, "9.png"));
        exampleDeck.add(new Card(Suit.PIKE, 10, "10.png"));
        exampleDeck.add(new Card(Suit.HEART, 10, "11.png"));
        exampleDeck.add(new Card(Suit.TILE, 10, "12.png"));

        exampleDeck.add(new Card(Suit.CLOVER, 10, "13.png"));
        exampleDeck.add(new Card(Suit.PIKE, 10, "14.png"));
        exampleDeck.add(new Card(Suit.HEART, 10, "15.png"));
        exampleDeck.add(new Card(Suit.TILE, 10, "16.png"));

        exampleDeck.add(new Card(Suit.CLOVER, 10, "17.png"));
        exampleDeck.add(new Card(Suit.PIKE, 10, "18.png"));
        exampleDeck.add(new Card(Suit.HEART, 10, "19.png"));
        exampleDeck.add(new Card(Suit.TILE, 10, "20.png"));

        exampleDeck.add(new Card(Suit.CLOVER, 9, "21.png"));
        exampleDeck.add(new Card(Suit.PIKE, 9, "22.png"));
        exampleDeck.add(new Card(Suit.HEART, 9, "23.png"));
        exampleDeck.add(new Card(Suit.TILE, 9, "24.png"));

        exampleDeck.add(new Card(Suit.CLOVER, 8, "25.png"));
        exampleDeck.add(new Card(Suit.PIKE, 8, "26.png"));
        exampleDeck.add(new Card(Suit.HEART, 8, "27.png"));
        exampleDeck.add(new Card(Suit.TILE, 8, "28.png"));

        exampleDeck.add(new Card(Suit.CLOVER, 7, "29.png"));
        exampleDeck.add(new Card(Suit.PIKE, 7, "30.png"));
        exampleDeck.add(new Card(Suit.HEART, 7, "31.png"));
        exampleDeck.add(new Card(Suit.TILE, 7, "32.png"));

        exampleDeck.add(new Card(Suit.CLOVER, 6, "33.png"));
        exampleDeck.add(new Card(Suit.PIKE, 6, "34.png"));
        exampleDeck.add(new Card(Suit.HEART, 6, "35.png"));
        exampleDeck.add(new Card(Suit.TILE, 6, "36.png"));

        exampleDeck.add(new Card(Suit.CLOVER, 5, "37.png"));
        exampleDeck.add(new Card(Suit.PIKE, 5, "38.png"));
        exampleDeck.add(new Card(Suit.HEART, 5, "39.png"));
        exampleDeck.add(new Card(Suit.TILE, 5, "40.png"));

        exampleDeck.add(new Card(Suit.CLOVER, 4, "41.png"));
        exampleDeck.add(new Card(Suit.PIKE, 4, "42.png"));
        exampleDeck.add(new Card(Suit.HEART, 4, "43.png"));
        exampleDeck.add(new Card(Suit.TILE, 4, "44.png"));

        exampleDeck.add(new Card(Suit.CLOVER, 3, "45.png"));
        exampleDeck.add(new Card(Suit.PIKE, 3, "46.png"));
        exampleDeck.add(new Card(Suit.HEART, 3, "47.png"));
        exampleDeck.add(new Card(Suit.TILE, 3, "48.png"));

        exampleDeck.add(new Card(Suit.CLOVER, 2, "49.png"));
        exampleDeck.add(new Card(Suit.PIKE, 2, "50.png"));
        exampleDeck.add(new Card(Suit.HEART, 2, "51.png"));
        exampleDeck.add(new Card(Suit.TILE, 2, "52.png"));

        exampleDeck.sort(Comparator.comparing(Card::getSuit));
        return exampleDeck;
    }

}
