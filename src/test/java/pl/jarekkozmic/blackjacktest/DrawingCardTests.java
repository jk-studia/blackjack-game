package pl.jarekkozmic.blackjacktest;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pl.jarekkozmic.blackjack.models.Croupier;
import pl.jarekkozmic.blackjack.models.Deck;
import pl.jarekkozmic.blackjack.models.Player;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DrawingCardTests {

    private static Deck deck;

    @BeforeAll
    public static void setUp(){
        deck = new Deck();
        deck.shuffle();
    }
    @Test
    public void testIfPlayerDrawingCardWorks(){
        Player player = new Player("Jarek");
        deck = player.drawCards(deck);
        assertEquals(1, player.getHand().size());
        assertTrue(deck.getCards().size()<52);
    }

    @Test
    public void testIfCroupierDrawingCardWorks(){
        Croupier croupier = new Croupier();
        deck = croupier.drawCards(deck);
        assertEquals(2,croupier.getHand().size());
        assertTrue(deck.getCards().size()<51);
        assertTrue(croupier.getScore()!=croupier.getFullScore());
    }
}
