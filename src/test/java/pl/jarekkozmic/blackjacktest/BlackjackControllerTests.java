package pl.jarekkozmic.blackjacktest;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import pl.jarekkozmic.blackjack.controllers.BlackjackController;
import pl.jarekkozmic.blackjack.models.Player;
import org.testfx.api.FxRobot;

import java.io.IOException;


import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.base.WindowMatchers.isNotFocused;

import static org.testfx.matcher.control.LabeledMatchers.hasText;

@ExtendWith(ApplicationExtension.class)
public class BlackjackControllerTests {

    @Start
    public void start(Stage stage){
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/blackjack.fxml"));
        Player testPlayer = new Player("Test");
        BlackjackController blackjackController = new BlackjackController(testPlayer);
        loader.setController(blackjackController);
        try {
            stage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.setResizable(false);
        stage.show();
        stage.toFront();
    }


    @Test
    public void testIfBetWorks(FxRobot robot) {
        robot.clickOn("#betTextField");
        robot.eraseText(1);
        robot.write("500");
        robot.clickOn("#betButton");
        verifyThat("#amountOfCreditLabel",hasText("4500.0"));
    }


    @Test
    public void testIncorrectBet(FxRobot robot){
        Stage stage = (Stage) robot.lookup("#betButton").query().getScene().getWindow();
        robot.clickOn("#betButton");
        verifyThat(stage, isNotFocused());
        verifyThat("#messageLabel", hasText("Incorrect bet"));
        robot.clickOn("#okButton");
        robot.clickOn("#betTextField");
        robot.eraseText(1);
        robot.write("-100");
        robot.clickOn("#betButton");
        verifyThat(stage, isNotFocused());
        verifyThat("#messageLabel", hasText("Incorrect bet"));
    }

    @Test
    public void shouldLoseAfterHitting(FxRobot robot){
        robot.clickOn("#betTextField");
        robot.eraseText(10);
        robot.write("200");
        robot.clickOn("#betButton");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for(;;) {
            robot.clickOn("#hitButton");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Label scoreNumberLabel = robot.lookup("#scoreNumberLabel").query();
            int score = Integer.parseInt(scoreNumberLabel.getText());
            if (score > 21) {
                verifyThat("#informationLabel", hasText("YOU LOSE"));
                return;
            }
        }
    }

    @Test
    public void shouldLoseAfterStanding(FxRobot robot){
        for(;;) {
          robot.clickOn("#betTextField");
          robot.eraseText(10);
          robot.write("200");
          robot.clickOn("#betButton");
          try {
               Thread.sleep(2000);
           } catch (InterruptedException e) {
              e.printStackTrace();
           }
            robot.clickOn("#standButton");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Label scoreNumberLabel = robot.lookup("#scoreNumberLabel").query();
            int score = Integer.parseInt(scoreNumberLabel.getText());
            Label croupierScoreNumberLabel = robot.lookup("#croupierScoreNumberLabel").query();
            int croupierScoreNumber= Integer.parseInt(croupierScoreNumberLabel.getText());
            if (croupierScoreNumber > score) {
                verifyThat("#informationLabel", hasText("YOU LOSE"));
                return;
            }
        }
    }

    @Test
    public void shouldWinAfterStanding(FxRobot robot){
        for(;;) {
            robot.clickOn("#betTextField");
            robot.eraseText(10);
            robot.write("200");
            robot.clickOn("#betButton");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            robot.clickOn("#standButton");
            Label scoreNumberLabel = robot.lookup("#scoreNumberLabel").query();
            int score = Integer.parseInt(scoreNumberLabel.getText());
            Label croupierScoreNumberLabel = robot.lookup("#croupierScoreNumberLabel").query();
            int croupierScoreNumber= Integer.parseInt(croupierScoreNumberLabel.getText());
            if (croupierScoreNumber < score && score<22) {
                verifyThat("#informationLabel", hasText("YOU WIN"));
                return;
            }
        }
    }
}
