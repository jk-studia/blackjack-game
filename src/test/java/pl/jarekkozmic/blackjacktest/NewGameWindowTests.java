package pl.jarekkozmic.blackjacktest;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

import java.io.IOException;

import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.control.LabeledMatchers.hasText;
import static org.testfx.matcher.base.WindowMatchers.isNotShowing;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.testfx.api.FxRobot;

@ExtendWith(ApplicationExtension.class)
public class NewGameWindowTests  {

    private Stage stage;
    @Start
    public void start(Stage stage){
        Parent rootTest = null;
        try {
            rootTest = FXMLLoader.load(getClass().getResource("/views/newGameWindow.fxml"));
            stage.setScene(new Scene(rootTest));
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.show();
        stage.toFront();
        this.stage = stage;
    }


    @Test
    public void testIfContainsButton(FxRobot robot){
        verifyThat("#startButton", hasText("Start"));
    }

    @Test
    public void testIfNewGameWindowWorks(FxRobot robot){
        robot.clickOn("#nameField");
        robot.eraseText(6);
        robot.write("Test Robot");
        TextField textField = robot.lookup("#nameField").query();
        assertEquals(textField.getText(), "Test Robot");
        robot.clickOn("#startButton");
        verifyThat(stage, isNotShowing());
    }

    @Test
    public void testError(FxRobot robot){
        robot.clickOn("#nameField");
        robot.eraseText(6);
        robot.clickOn("#startButton");
        verifyThat("#messageLabel", hasText("Name must be specified"));
    }

}
