# Blackjack

A single player card game. Your starting credit: 5000.

<img src="images/blackjackScreen.png"/>

Setting up project:

- Java 8:
    1. Make sure you're using JRE 8 and JDK 8 with a built-in JavaFX (contains external library jfxrt.jar, you can download it from Oracle website, version 8u212)
    2. Clone repository.
    3. (When using IDE) Import project from maven.
    4. Create folder "cards" in "resources" directory and paste there downloaded card images from the link.
    5. (To save player in database, not necessary to play) Create folder "META-INF" in "resources" directory and create there hibernate.cfg.xml file with your own configuration of database connection.
    6. Run program.
    
- Java 11
    1. Make sure you're using JRE 11 and JDK 11.
    2. Follow steps 2-5 from previous version.
    3. Replace default pom.xml with pom.xml in j11 directory.
    4. After compilation run program with options (or add them to Run Configurations in your IDE): --module-path ${YOUR_PATH_TO_OPENJFX_LIB} --add-modules javafx.controls,javafx.fxml

Used technologies:

1. Javafx 
2. JUnit5
3. TestFX
4. Hibernate
5. Maven

Link to card images: <https://opengameart.org/content/playing-cards>